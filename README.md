# Plugin Registro Usuario Custom Wordpress

Plugin para registrar de manera custom a usuarios de WordPress, notificarles, logearlo de manera única. Repositorio privado https://gitlab.com/diurvan/diu-register-user

![Imagen](https://gitlab.com/diurvan/plugin-registro-usuario-custom-wordpress/-/raw/main/RegistroUsuario1.png)
![Imagen](https://gitlab.com/diurvan/plugin-registro-usuario-custom-wordpress/-/raw/main/RegistroUsuario2.png)
![Imagen](https://gitlab.com/diurvan/plugin-registro-usuario-custom-wordpress/-/raw/main/RegistroUsuario3.png)
![Imagen](https://gitlab.com/diurvan/plugin-registro-usuario-custom-wordpress/-/raw/main/RegistroUsuario4.png)
![Imagen](https://gitlab.com/diurvan/plugin-registro-usuario-custom-wordpress/-/raw/main/RegistroUsuario5.png)
![Imagen](https://gitlab.com/diurvan/plugin-registro-usuario-custom-wordpress/-/raw/main/RegistroUsuario6.png)

Contribuye con tus comentarios.

Visita mi web en https://diurvanconsultores.com
O escríbeme a ivan.tapia@diurvanconsultores.com
